package br.ufrn.imd.models;

import br.ufrn.imd.models.BankAccount;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Quando;

import javax.naming.InsufficientResourcesException;

import java.util.function.DoubleUnaryOperator;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class BankAccountStepdefs {

    private BankAccount bankAccount;
    private BankAccount bankDestinationAccount;

    @Given("um BankAccount com saldo de R$ {double}")
    public void um_bank_account_com_saldo_de_r$(Double double1) {
        //TODO
        bankAccount = new BankAccount(123456, 123, double1);
    }

    @When("depositar R$ {double}")
    public void depositar_r$(Double double1) {
        //TODO
        bankAccount.deposit(double1);
    }

    @Then("o saldo do BankAccount deve ser R$ {double}")
    public void o_saldo_deve_ser_r$(Double double1) {
        //TODO
        var saldo = bankAccount.getBalance();
        assertEquals(double1, saldo);
    }

    @When("retirar R$ {double}")
    public void retirarR$(Double double1) throws InsufficientResourcesException {
        bankAccount.withdraw(double1);
    }

    @And("um BankAccount de destino com saldo de R$ {double}")
    public void umBankAccountDeDestinoComSaldoDeR$(Double double1) {
        bankDestinationAccount = new BankAccount(654321, 321, double1);
    }

    @When("tranferir R$ {double} para o BankAccount de destino")
    public void tranferirR$ParaOBankAccountDeDestino(Double double1) throws InsufficientResourcesException {
        bankAccount.transfer(bankDestinationAccount, double1);
    }

    @And("o saldo do BankAccount de destino deve ser R$ {double}")
    public void oSaldoDoBankAccountDeDestinoDeveSerR$(Double double1) {
       var saldoContaDestino = bankDestinationAccount.getBalance();
        assertEquals(double1, saldoContaDestino);
    }
}
